import axios from 'axios';
import config from '@/store/config.json';
import { hCommit } from '@/store/helpres/hMethods';

import {
  MUTATION_SET_SETTINGS,
  MUTATION_SET_FIELD,
} from '@/store';
import { TYPE_SMS } from '@/store/helpres/types'

const actions = {
  // eslint-disable-next-line
  resetFields({ commit, state }) {
    for (let key in state.fields) {
      commit(MUTATION_SET_FIELD, {
        name: key,
        value: '',
      });
    }
  },
  // eslint-disable-next-line
  getSMSCode({ commit, state }, telephone) {
    return new Promise((resolve) => {
      setTimeout(() => {
        resolve({
          // 'code': Math.floor(1000 + Math.random() * 9000)
          // eslint-disable-next-line
          'code': 1111,
        });
      }, 300);
    });
  },
  // eslint-disable-next-line
  getPromoCode({ commit, state }, code) {
    return new Promise((resolve) => {
      setTimeout(() => {
        if (code.length === 0) {
          resolve({
            status: 'empty',
          });
        }

        if (code === 'ABC123') {
          resolve({
            status: 'success',
          });
        } else {
          resolve({
            status: 'fail',
          });
        }
      }, 300);
    });
  },
  // eslint-disable-next-line
  cardCreate({ commit, state }, result) {
    return new Promise((resolve) => {
      setTimeout(() => {
        resolve({
          status: 'success',
        });
      }, 5000);
    });
  },
  // eslint-disable-next-line
  getConfig({ commit, state }) {
    return axios.get('https://tested.smartleo.ru/uploads/config.json')
    // new Promise(resolve => {
    //   resolve({
    //     data: config,
    //   });
    // })
      .then(({ data }) => {
        commit(MUTATION_SET_SETTINGS, hCommit(data.locale, 'locale', 'ru'))
        commit(MUTATION_SET_SETTINGS, hCommit(data.theme, 'theme', 'day'));
        commit(MUTATION_SET_SETTINGS, hCommit(data.formType, 'formType', TYPE_SMS));
        commit(MUTATION_SET_SETTINGS, hCommit(data.formFields, 'formFields'));
        commit(MUTATION_SET_SETTINGS, hCommit(data.social, 'social', null));
        commit(MUTATION_SET_SETTINGS, hCommit(data.title, 'title', 'CardPr'));

        commit(MUTATION_SET_SETTINGS, {
          name: 'description',
          value: data.description,
        });

        commit(MUTATION_SET_SETTINGS, {
          name: 'background',
          value: data.background,
        });
      });
  },
};

export default actions;
