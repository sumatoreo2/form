import { formTypes } from '@/store/helpres/types';

const getters = {
  locale: state => state.settings.locale,
  formType: state => state.settings.formType,
  steps: state => formTypes[state.settings.formType],
  title: state => state.settings.title,
  formFields: state => state.settings.formFields.sort((prev, next) => prev.index > next.index),
  social: state => state.settings.social,
  firstVisit: state => state.firstVisit,
  showDescription: state => state.settings.description.show,
};

export default getters;