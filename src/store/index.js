import Vue from 'vue';
import Vuex from 'vuex';
import createPersistedState from 'vuex-persistedstate';

import mutations from '@/store/mutations';
import actions from '@/store/actions';
import getters from '@/store/getters';
import {TYPE_SMS} from '@/store/helpres/types'

Vue.use(Vuex);

export const MUTATION_INCREMENT_STEP = 'incrementStep';
export const MUTATION_DECREMENT_STEP = 'decrementStep';
export const MUTATION_SET_STEP = 'setStep';
export const MUTATION_DESCRIPTION_VISIBILITY = 'descriptionVisibility';
export const MUTATION_SET_FIELD = 'setField';
export const MUTATION_SET_SETTINGS = 'setSettings';
export const MUTATION_SET_STATE = 'setState';

export const ACTION_GET_SMS_CODE = 'getSMSCode';
export const ACTION_GET_PROMO_CODE = 'getPromoCode';
export const ACTION_CARD_CREATE = 'cardCreate';
export const ACTION_GET_CONFIG = 'getConfig';
export const ACTION_RESET_FIELDS = 'resetFields';

export const GETTER_LOCALE = 'locale';
export const GETTER_FORM_TYPE = 'formType';
export const GETTER_STEPS = 'steps';
export const GETTER_TITLE = 'title';
export const GETTER_FORM_FIELDS = 'formFields';
export const GETTER_SOCIAL = 'social';
export const GETTER_FIRST_VISIT = 'firstVisit';
export const GETTER_SHOW_DESCRIPTION = 'showDescription';

export default new Vuex.Store({
  plugins: [createPersistedState({ storage: window.sessionStorage })],
  state: {
    fields: {},
    firstVisit: true,
    settings: {
      description: {
        show: true,
        logo: '<span style="font-size: 24px; font-weight: bold; letter-spacing: 2px; color: #ff5700; text-shadow: 1px 1px 0 rgba(0, 0, 0, 1);">Carbonara</span>',
        subtitle: 'Бонусная карта <br/>«Carbonara Club»',
        text: '10% от суммы каждого счета возвращаются на карту. Оплачивать бонусами можно любой заказ в ресторане «Carbonara», ресторане «Буфет и Пекарня» и детском игровом клубе «Маленькая Нарния».',
      },
      locale: 'ru',
      theme: 'night',
      background: 'https://form.smartleo.ru/img/background.d97cfd49.png',
      steps: TYPE_SMS,
      stepPosition: 0,
    },
  },
  mutations,
  actions,
  getters
});
