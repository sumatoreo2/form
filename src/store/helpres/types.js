export const TYPE_SMS = 'sms';
export const TYPE_FORM = 'form';

export const formTypes = {
  [TYPE_SMS]: [
    {
      name: 'sms-verification',
    },
    {
      name: 'application-form',
      /**
       * show - boolean
       * Если true, то полоска с шагами видима
       * Если false, то полоска с шагами невидима
       *
       * consider - boolean
       * Если true, то этот шаг будет учитываться при подсчете
       * Если false, то нет
       */
      show: true,
      consider: true,
    },
    {
      name: 'card-request',
      show: true,
      consider: true,
    },
    {
      name: 'qr-link',
      show: true,
      consider: true,
    },
    {
      name: 'card-ready',
      show: true,
    },
  ],
  [TYPE_FORM]: [
    {
      name: 'application-form',
      /**
       * show - boolean
       * Если true, то полоска с шагами видима
       * Если false, то полоска с шагами невидима
       *
       * consider - boolean
       * Если true, то этот шаг будет учитываться при подсчете
       * Если false, то нет
       */
      show: true,
      consider: true,
    },
    {
      name: 'card-request',
      show: true,
      consider: true,
    },
    {
      name: 'qr-link',
      show: true,
      consider: true,
    },
    {
      name: 'card-ready',
      show: true,
    },
  ],
};