/**
 * Commit helper
 * @param commit
 * @param value
 * @param name
 * @param defaultValue
 */
const hCommit = (value, name, defaultValue = null) => {
  if (value) {
    return {
      name: name,
      value: value,
    };
  } else if (defaultValue) {
    return {
      name: name,
      value: defaultValue,
    };
  }
};

export { hCommit };