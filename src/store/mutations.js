const mutations = {
  incrementStep(state) {
    // eslint-disable-next-line
    state.settings.stepPosition++;
  },
  decrementStep(state) {
    // eslint-disable-next-line
    state.settings.stepPosition--;
  },
  setStep(state, pos) {
    // eslint-disable-next-line
    if (pos >= 0) state.settings.stepPosition = pos;
  },
  descriptionVisibility(state, value) {
    // eslint-disable-next-line
    state.settings.description.show = value;
  },
  /**
   * Set field into state
   * @param state
   * @param data
   */
  setState(state, data) {
    state[data.name] = data.value;
  },
  /**
   * Set Field
   * @param state
   * @param field { name: 'telephone', value: '+7 (927) 999-99-99'}
   */
  setField(state, field) {
    // eslint-disable-next-line
    state.fields[field.name] = field.value;
  },
  /**
   * Set Settings Field
   * @param state
   * @param field { name: 'locale', value: 'en'}
   */
  setSettings(state, field) {
    // eslint-disable-next-line
    state.settings[field.name] = field.value;
  }
};

export default mutations;
