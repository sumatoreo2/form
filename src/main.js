import Vue from 'vue';
import vuexI18n from 'vuex-i18n';

import store, { ACTION_GET_CONFIG, GETTER_LOCALE } from './store';
import CardUi from './plugins/CardUi';

import App from './App.vue';
import './assets/less/main.less';

import _ru from './resources/i18n/ru';
import _en from './resources/i18n/en';

Vue.config.productionTip = false;

Vue.use(vuexI18n.plugin, store);
Vue.use(CardUi);

Vue.i18n.add('ru', _ru);
Vue.i18n.add('en', _en);

store.dispatch(ACTION_GET_CONFIG).then(() => {
  Vue.i18n.set(store.getters[GETTER_LOCALE]);

  new Vue({
    store,
    render: h => h(App),
  }).$mount('#app');
});