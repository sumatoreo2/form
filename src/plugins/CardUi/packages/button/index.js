const CButton = () => import('./src/button.vue');

// eslint-disable-next-line
CButton.install = function (Vue) {
  Vue.component(CButton.name, CButton);
};

export default CButton;
