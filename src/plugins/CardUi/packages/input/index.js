const CInput = () => import('./src/input.vue');

// eslint-disable-next-line
CInput.install = function (Vue) {
  Vue.component(CInput.name, CInput);
};

export default CInput;
