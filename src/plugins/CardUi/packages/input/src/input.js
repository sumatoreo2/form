import IMask from 'imask';

export default {
  name: 'c-input',
  data() {
    return {
      focused: false,
      inputMaskInputElement: null,
      mask: null,
    };
  },
  inheritAttrs: false,
  props: {
    type: {
      type: String,
      required: true,
      validator(value) {
        return ['tel', 'text', 'code', 'password', 'email'].indexOf(value) !== -1;
      },
    },
    focus: Boolean,
    value: [String, Number],
    placeholder: String,
    disabled: Boolean,
    readonly: Boolean,
    isValid: Boolean,
    errorMessage: String,
    action: {
      type: Boolean,
      default: false,
    }
  },
  mounted() {
    if (this.type === 'tel') this.telephoneMask();
    if (this.type === 'code') this.codeMask();

    if (this.focus) {
      this.$nextTick(() => {
        this.$refs.input.focus();
      });
    }
  },
  computed: {
    readonlyFix() {
      if (this.readonly) {
        return { 'user-select': 'none' };
      } else {
        return {};
      }
    },
    hidePlaceholder() {
      return this.focused || this.value;
    },
    inputListeners() {
      const vm = this;

      return Object.assign(
        {},
        this.$listeners,
        {
          input(event) {
            if (vm.mask) {
              event.target.value = vm.mask.resolve(event.target.value);
            }

            vm.$emit('input', event.target.value);
          },
          change(event) {
            vm.$emit('change', event);
          },
          focus(event) {
            vm.focused = true;

            vm.$emit('focus', event);
          },
          blur(event) {
            vm.focused = false;

            vm.$emit('blur', event);
          },
        },
      );
    },
  },
  methods: {
    codeMask() {
      this.mask = IMask.createMask({
        mask: [{
          mask: '0000',
          lazy: true,
          expression: /[\d+]{4}/,
        }]
      });

      this.$refs.input.value = this.mask.resolve(this.value);
    },
    telephoneMask() {
      this.mask = IMask.createMask({
          mask: [
            {
              mask: '+{38} (000) 000-00-00',
              expression: /\+38\s\([\d]{3}\)\s[\d]{3}-[\d]{2}-[\d]{2}/,
              startsWith: '38',
              lazy: true,
              country: 'Ua'
            },
            {
              mask: '+{7} (000) 000-00-00',
              expression: /\+7\s\([\d]{3}\)\s[\d]{3}-[\d]{2}-[\d]{2}/,
              startsWith: '7',
              lazy: true,
              country: 'Ru'
            },
            {
              mask: '8 (000) 000-00-00',
              expression: /8\s\([\d]{3}\)\s[\d]{3}-[\d]{2}-[\d]{2}/,
              startsWith: '8',
              lazy: true,
              country: 'Ru'
            },
            {
              mask: '0000000000000',
              startsWith: '',
              country: 'unknown'
            },
          ],
          dispatch: function (appended, dynamicMasked) {
            const number = (dynamicMasked.value + appended).replace(/\D/g, '');

            const res = dynamicMasked.compiledMasks.find(function (m) {
              return number.indexOf(m.startsWith) === 0;
            });

            return res;
          }
        }
      );

      if (this.value) {
        this.$refs.input.value = this.mask.resolve(this.value);
      }
    },
    getMask() {
      return this.mask;
    },
    focusMethod() {
      this.$refs.input.focus();
    },
  },
  destroyed() {
    this.mask = null;
  }
};
