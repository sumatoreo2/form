const CCheckbox = () => import('./src/checkbox.vue');

// eslint-disable-next-line
CCheckbox.install = function (Vue) {
  Vue.component(CCheckbox.name, CCheckbox);
};

export default CCheckbox;
