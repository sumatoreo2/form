const CLoader = () => import('./src/loader.vue');

// eslint-disable-next-line
CLoader.install = function (Vue) {
  Vue.component(CLoader.name, CLoader);
};

export default CLoader;
