// eslint-disable-next-line
import Input from './packages/input/src/input.vue';

// eslint-disable-next-line
import Button from './packages/button/src/button.vue';

// eslint-disable-next-line
import Checkbox from './packages/checkbox/src/checkbox.vue';

// eslint-disable-next-line
import Loader from './packages/loader/src/loader.vue';

const components = [
  Input,
  Button,
  Loader,
  Checkbox,
];

const install = (Vue) => {
  components.forEach((component) => {
    Vue.component(component.name, component);
  });
};

export default {
  version: '1.0.0',
  install,
  Input,
  Button,
  Loader,
  Checkbox,
};

